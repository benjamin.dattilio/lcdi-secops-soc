#!/bin/bash
if [ `whoami` != 'root' ]; 
  then
   echo Must be run as root! 
   exit 100
fi
echo ----------Stopping Services----------
systemctl stop kibana.service
echo kibana stopped
systemctl stop elasticsearch.service
echo elasticsearch stopped
systemctl stop logstash.service
echo logstash stopped
echo ----------Starting Services----------
systemctl start kibana.service
echo Starting kibana
systemctl start elasticsearch.service
echo Starting elasticsearch
systemctl start logstash.service
echo Starting logstash
echo ----------Service Check----------
systemctl is-active --quiet kibana.service && echo kibana.service is running
systemctl is-active --quiet elasticsearch.service && echo elasticsearch.service is running 
systemctl is-active --quiet logstash.service && echo logstash.service is running


